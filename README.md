## Sales taxes problem

Si tratta di un progetto Maven realizzato in Java, per testarlo è sufficiente:

1. Importare su un IDE (le istruzioni sono per Eclipse, ma vanno bene anche NetBeans, IntelliJ, ecc) come Existing Maven Project.
2. Fare tasto destro sul progetto -> Maven -> Update Project e dare OK per recuperare le dipendenze (in questo caso solo JUnit).
3. Fare un clean/build al progetto.
4. Lanciare come JUnit Test le classi in src/test/java.