/**
 * 
 */
package montibeller.model;

/**
 * @author Giacomo Montibeller
 *
 */
public abstract class Product {

	private int quantity;
	private String name;
	private double price;
	private double taxValue;
	private double totalPrice;

	/**
	 * 
	 */
	public Product() {
	}

	/**
	 * @param quantity
	 * @param name
	 * @param price
	 */
	public Product(int quantity, String name, double price) {
		this.setQuantity(quantity);
		this.setName(name);
		this.setPrice(price);

		this.calculateTotalPrice();
	}

	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * 
	 * @return the taxValue
	 */
	public double getTaxValue() {
		return taxValue;
	}

	/**
	 * 
	 * @param taxValue the taxValue to set
	 */
	public void setTaxValue(double taxValue) {
		this.taxValue = taxValue;
	}

	/**
	 * @return the totalPrice
	 */
	public double getTotalPrice() {
		return totalPrice;
	}

	/**
	 * @param totalPrice the totalPrice to set
	 */
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * 
	 */
	public abstract void calculateTotalPrice();
}
