/**
 * 
 */
package montibeller.model;

import montibeller.util.Constants;

/**
 * @author Giacomo Montibeller
 *
 */
public class ProductExemptImported extends Product {

	/**
	 * 
	 */
	public ProductExemptImported() {
		super();
	}

	/**
	 * 
	 * @param quantity
	 * @param name
	 * @param price
	 */
	public ProductExemptImported(int quantity, String name, double price) {
		super(quantity, name, price);
	}

	/**
	 * 
	 */
	public void calculateTotalPrice() {
		double untaxedPrice = this.getQuantity() * this.getPrice();

		double taxValue = (untaxedPrice * (100 + Constants.IMPORTED_TAX) / 100) - untaxedPrice;

		double taxRounded = Math.round(taxValue * 20.0) / 20.0;

		double taxedPrice = Math.round((untaxedPrice + taxRounded) * 100.0) / 100.0;

		this.setTaxValue(taxRounded);
		this.setTotalPrice(taxedPrice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = this.getQuantity() + " " + Constants.IMPORTED + " " + this.getName() + ": "
				+ this.getTotalPrice();

		return result;
	}

}
