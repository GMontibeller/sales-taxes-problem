/**
 * 
 */
package montibeller.model;

/**
 * @author Giacomo Montibeller
 *
 */
public class ProductExemptNotImported extends Product {

	/**
	 * 
	 */
	public ProductExemptNotImported() {
		super();
	}

	/**
	 * 
	 * @param quantity
	 * @param name
	 * @param price
	 */
	public ProductExemptNotImported(int quantity, String name, double price) {
		super(quantity, name, price);
	}

	/**
	 * 
	 */
	public void calculateTotalPrice() {
		double totalPrice = Math.round((this.getQuantity() * this.getPrice()) * 100.0) / 100.0;

		this.setTaxValue(0);
		this.setTotalPrice(totalPrice);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = this.getQuantity() + " " + this.getName() + ": " + this.getTotalPrice();

		return result;
	}

}
