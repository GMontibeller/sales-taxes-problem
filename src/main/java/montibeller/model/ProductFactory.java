/**
 * 
 */
package montibeller.model;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Giacomo Montibeller
 *
 */
public class ProductFactory {

	public Product getProduct(int quantity, String name, double price, boolean isImported) {

		List<String> trainingSet = new LinkedList<String>();
		boolean isExempt = false;

		// simple set to test
		trainingSet.add("book");
		trainingSet.add("chocolate");
		trainingSet.add("headache");

		for (String s : trainingSet) {
			if (name.toLowerCase().contains(s.toLowerCase())) {
				isExempt = true;
				break;
			}
		}

		if (isExempt && isImported) {
			return new ProductExemptImported(quantity, name, price);
		} else if (isExempt && !isImported) {
			return new ProductExemptNotImported(quantity, name, price);
		} else if (!isExempt && isImported) {
			return new ProductNotExemptImported(quantity, name, price);
		} else {
			return new ProductNotExemptNotImported(quantity, name, price);
		}
	}
}
