/**
 * 
 */
package montibeller.util;

/**
 * @author Giacomo Montibeller
 *
 */
public class Constants {

	public static final String WHITE_SPACE = " ";
	public static final String IMPORTED = "imported";
	public static final int NOT_EXEMPT_TAX = 10;
	public static final int IMPORTED_TAX = 5;

}
