/**
 * 
 */
package montibeller.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Giacomo Montibeller
 *
 */
public class Util {

	/**
	 * 
	 * @param input
	 * @return
	 */
	public static Map<String, Object> splitInput(String input) {
		String[] splittedInput;
		int quantity;
		String name;
		double price;

		int i = 1;
		boolean imported = false;
		StringBuilder builder = new StringBuilder();
		Map<String, Object> result = new HashMap<String, Object>();

		splittedInput = input.trim().split("\\s+");

		if (Constants.IMPORTED.equalsIgnoreCase(splittedInput[1])) {
			imported = true;
			i = 2;
		}

		for (; i < splittedInput.length - 2; i++) {
			builder.append(splittedInput[i]);
			builder.append(Constants.WHITE_SPACE);
		}

		quantity = Integer.parseInt(splittedInput[0]);
		name = builder.toString().trim();
		price = Double.parseDouble(splittedInput[splittedInput.length - 1]);

		result.put("quantity", quantity);
		result.put("name", name);
		result.put("price", price);
		result.put("imported", imported);

		return result;
	}
}
