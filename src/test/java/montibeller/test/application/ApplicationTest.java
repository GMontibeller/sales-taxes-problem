/**
 * 
 */
package montibeller.test.application;

import static org.junit.Assert.assertEquals;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import montibeller.model.Product;
import montibeller.model.ProductFactory;
import montibeller.util.Util;

/**
 * @author Giacomo Montibeller
 *
 */
public class ApplicationTest {

	private List<String> testInput;
	private Map<String, Object> testInputSplitted;
	private ProductFactory factory = new ProductFactory();
	private double totalTax;
	private double totalPrice;

	private void test() {
		for (String test : testInput) {
			testInputSplitted = Util.splitInput(test);

			int quantity = (Integer) testInputSplitted.get("quantity");
			String name = (String) testInputSplitted.get("name");
			double price = (Double) testInputSplitted.get("price");
			boolean isImported = (Boolean) testInputSplitted.get("imported");

			Product prod = factory.getProduct(quantity, name, price, isImported);

			totalTax += prod.getTaxValue();
			totalPrice += prod.getTotalPrice();

			System.out.println(prod.toString());
		}
	}

	@Test
	public void input1() {
		totalTax = 0.0;
		totalPrice = 0.0;
		testInput = new LinkedList<String>();

		testInput.add("2 book at 12.49");
		testInput.add("1 music CD at 14.99");
		testInput.add("1 chocolate bar at 0.85");

		System.out.println("Output 1:");

		test();

		assertEquals(1.50, totalTax, 0.0);
		assertEquals(42.32, totalPrice, 0.0);

		System.out.println("Sales Taxes: " + totalTax);
		System.out.println("Total: " + totalPrice);
		System.out.println();
	}

	@Test
	public void input2() {
		totalTax = 0.0;
		totalPrice = 0.0;
		testInput = new LinkedList<String>();

		testInput.add("1 imported box of chocolates at 10.00");
		testInput.add("1 imported bottle of perfume at 47.50");

		System.out.println("Output 2:");

		test();

		assertEquals(7.65, totalTax, 0.0);
		assertEquals(65.15, totalPrice, 0.0);

		System.out.println("Sales Taxes: " + totalTax);
		System.out.println("Total: " + totalPrice);
		System.out.println();
	}

	@Test
	public void input3() {
		totalTax = 0.0;
		totalPrice = 0.0;
		testInput = new LinkedList<String>();

		testInput.add("1 imported bottle of perfume at 27.99");
		testInput.add("1 bottle of perfume at 18.99");
		testInput.add("1 packet of headache pills at 9.75");
		testInput.add("3 imported box of chocolates at 11.25");

		System.out.println("Output 3:");

		test();

		assertEquals(7.80, totalTax, 0.0);
		assertEquals(98.28, totalPrice, 0.0);

		System.out.println("Sales Taxes: " + totalTax);
		System.out.println("Total: " + totalPrice);
	}
}
