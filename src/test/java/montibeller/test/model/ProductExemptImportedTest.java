/**
 * 
 */
package montibeller.test.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import montibeller.model.ProductExemptImported;

/**
 * @author Giacomo Montibeller
 *
 */
public class ProductExemptImportedTest {

	@Test
	public void createProduct() {
		int quantity = 3;
		String name = "chocolate bars";
		double price = 2.42;

		ProductExemptImported prod = new ProductExemptImported(quantity, name, price);

		assertEquals(quantity, prod.getQuantity());
		assertEquals(name, prod.getName());
		assertEquals(price, prod.getPrice(), 0.0);
	}

	@Test
	public void calculateTotalPrice() {
		ProductExemptImported prod = new ProductExemptImported();

		prod.setQuantity(2);
		prod.setPrice(5);

		prod.calculateTotalPrice();
		assertEquals(10.5, prod.getTotalPrice(), 0.0);
	}
}
