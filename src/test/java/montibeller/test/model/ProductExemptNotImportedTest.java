/**
 * 
 */
package montibeller.test.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import montibeller.model.ProductExemptNotImported;

/**
 * @author Giacomo Montibeller
 *
 */
public class ProductExemptNotImportedTest {

	@Test
	public void createProduct() {
		int quantity = 3;
		String name = "chocolate bars";
		double price = 2.42;

		ProductExemptNotImported prod = new ProductExemptNotImported(quantity, name, price);

		assertEquals(quantity, prod.getQuantity());
		assertEquals(name, prod.getName());
		assertEquals(price, prod.getPrice(), 0.0);
	}

	@Test
	public void calculateTotalPrice() {
		ProductExemptNotImported prod = new ProductExemptNotImported();

		prod.setQuantity(2);
		prod.setPrice(5);

		prod.calculateTotalPrice();
		assertEquals(10, prod.getTotalPrice(), 0.0);
	}
}
