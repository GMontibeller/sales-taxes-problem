/**
 * 
 */
package montibeller.test.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import montibeller.model.ProductNotExemptNotImported;

/**
 * @author Giacomo Montibeller
 *
 */
public class ProductNotExemptNotImportedTest {

	@Test
	public void createProduct() {
		int quantity = 3;
		String name = "chocolate bars";
		double price = 2.42;

		ProductNotExemptNotImported prod = new ProductNotExemptNotImported(quantity, name, price);

		assertEquals(quantity, prod.getQuantity());
		assertEquals(name, prod.getName());
		assertEquals(price, prod.getPrice(), 0.0);
	}

	@Test
	public void calculateTotalPrice() {
		ProductNotExemptNotImported prod = new ProductNotExemptNotImported();

		prod.setQuantity(2);
		prod.setPrice(5);

		prod.calculateTotalPrice();
		assertEquals(11, prod.getTotalPrice(), 0.0);
	}
}
