/**
 * 
 */
package montibeller.test.util;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

import montibeller.util.Util;

/**
 * @author Giacomo Montibeller
 *
 */
public class UtilTest {

	@Test
	public void splitInputTest() {
		Map<String, Object> inputSplitted;

		inputSplitted = Util.splitInput("12 imported cioccolato al latte a 12.49");

		assertEquals(12, inputSplitted.get("quantity"));
		assertEquals("cioccolato al latte", inputSplitted.get("name"));
		assertEquals(12.49, inputSplitted.get("price"));
		assertEquals(true, inputSplitted.get("imported"));
	}
}
